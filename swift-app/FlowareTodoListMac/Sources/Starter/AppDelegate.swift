//
//  AppDelegate.swift
//  FlowareTodoList
//
//  Created by Hung Truong on 6/12/19.
//  Copyright © 2019 Hung Truong. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

