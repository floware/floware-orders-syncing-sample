import { connect } from 'react-redux';
import TodoList from '../components/todo/TodoList';
import { VisibilityFilters } from '../constants/actionTypes'
import { store } from '../store';

const getVisibleTodos = (todos, filter) => {
  switch(filter) {
    case VisibilityFilters.SHOW_ALL:
      return todos;
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(t => t.completed);
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
  todos: getVisibleTodos(state.todoListReducer, state.visibilityFilter)
})

const mapDispatchToProps = dispatch => ({
  
})

export default connect(
  mapStateToProps,
  null
)(TodoList)