import authorizedReducer from './reducers/authorizedReducer';
import todoListReducer from './reducers/todoListReducer';
import visibilityFilter from './reducers/visibilityFilter';
import { combineReducers } from 'redux';


export default combineReducers ({
  authorizedReducer,
  todoListReducer,
  visibilityFilter
});
